package main

import "fmt"



const(
  a=iota
  b=iota
  c=iota
)

const(
  a=iota
  b
  c
)

const (
  a2=iota
)

func main()  {

    p:=fmt.Println

    a:=3
    b:=9
    p(a<<3)
    p(b>>2)


    g:="string"

    by:=[]byte(g)//byte slice converts to utf8
    g1:='r'//rune is a utf32 character
    p(g1)
    p(by)//uint8
    p(string(by))


    p(a)//0

    p(b)//1
    p(c)//2
    p(a2)//0



    array:= [4]int{45,56,67,77}
    p(array)

    array2:=[...]float32{4.5,5.8,7.8,9.0}
    p(array2)

    array3:=array
    p(array3)//45,56,67,77


    array3:=&array
    array3[0]=23
    array[1]=78

    p(array3)
    p(array)


    slc:=[]int{1,2,5,4,7,8,9}
    b:=slc[1:4]
    p(b)//[2,5,4]
    slc=append(slc,99)
    p(slc)

    sl:=make([]float32, 12)
    sl=append(sl,[]float32{3.8,7.9,0.9,0.6}...)

    map1:=map[string]int{
      "element1":90,
      "element2":97,
    }

    p(map1)

    delete(map1, "element2")
    p(map1)

   make(map[string]int,12)
    type str1 struct{
     a int16
     b int16
    }

    aDoctor:= struct {name string} {name: "rajath"}//anonymous struct
    p(aDoctor)

    a:=[]int{3,4,5,6,7}

    for k,v := range a{//loops through slice a using k,v
      p(k,v)
    }

    f:= func ()  {

      a:=[]int{3,4,5,6,7}

      for k,v := range a{//loops through slice a using k,v
        p(k,v)
      }

    }

    f()//anonymous func

}
