package main

//go get -> cannot download, /home/qtile/go is a GOROOT, not a GOPATH
//sol-> 1. export GOPATH="/home/qtile/go" export GOROOT="/usr/qtile/go"
//		2. cleaning GOPATH by rm -rf $GOPATH

import (
	"fmt"
	"strconv"
)

var (
	firstVal  int     = 7
	secondVal float32 = 6.9
)

var (
	temp  string = "hello"
	temp2 string = "world"
)

func main() {
	fmt.Println("hello")
	p := fmt.Println

	p(temp + temp2)
	fmt.Printf("%v %T", temp2, temp2)
	firstVal1 := float32(firstVal)
	fmt.Printf("%T", firstVal1)
	secondVal1 := int(secondVal)
	fmt.Printf("%T", secondVal1)
	tempToInt, _ := strconv.Atoi("temp")
	p(tempToInt)

}
